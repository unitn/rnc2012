#!/usr/bin/env ruby

module RNC
  AXES = {:X => 0, :Y => 1, :Z => 2 }
  COMMANDS = [:G00, :G01]
  
  class Point < Array
    # Defines a class method that allows to create a new Point instance by 
    # typing:
    #     p = Point[1,0,0]
    # instead of:
    #     p = Point.new [1,0,0]
    # @param [Numeric] x The X coordinate 
    # @param [Numeric] y The Y coordinate 
    # @param [Numeric] z The Z coordinate 
    # @return [Point] A new instance of a Point object
    def Point.[](x=nil, y=nil, z=nil)
      Point.new([x, y, z])
    end
    
    # This method allows to access one of the three coordinates as:
    #     p[:X]
    # @param [Fixnum|Symbol|String] index An axis index, as :X, "x", or 0.
    # @return [Numeric] The point coordinate
    def [](index)
      super(index_remap(index))
    end
    
    # This method allows to set a given coordinate as:
    #     p[:Z] = 100
    # which is interpreted by Ruby exactly as you would write:
    #     p.[]=(:Z, 100)
    # @param [Fixnum|Symbol|String] index An axis index, as :X, "x", or 0.
    # @param [Numeric] value The point coordinate to be set.
    def []=(index,value)
      super(index_remap(index), value)
    end
    
    # Copies the coordinates missing (i.e. nil) in self from values in other
    # @param [Point] other The point where to copy previous values from.
    def modal!(other)
      [:X, :Y, :Z].each do |k|
        self[k] = other[k] unless self[k]
      end
    end
    
    # Reurns the length of the segment between
    # self and other
    # @param [Point] other The other point
    def -(other)
      Math::sqrt(
        (self[:X] - other[:X]) ** 2 +
        (self[:Y] - other[:Y]) ** 2 +
        (self[:Z] - other[:Z]) ** 2 
      )
    end
    
    # Returns the three projections
    # @param [Point] other The starting point from where the three projections 
    #        are to be calculated
    def delta(other)
      Point[
        self[0] - other[0],
        self[1] - other[1],
        self[2] - other[2]
      ]
    end
    
    # Returns a nicely formatted point representation.
    # @return [String] The point object description
    def inspect
      "[#{self[:X] or '*'} #{self[:Y] or '*'} #{self[:Z] or '*'}]"
    end
    
    private
    # Remaps index (which could be "x", "X", :X, or 0) to an array index (i.e.
    # 0, 1, or 2) according to the map defined in the AXES hash.
    def index_remap(index)
      case index
      when Numeric
        index = index.to_i
      when String
        index = AXES[index.upcase.to_sym]
      when Symbol
        index = AXES[index]
      else
        raise ArgumentError
      end
      return index
    end
  end #class Point



  # Class converting a single line of G-Code to a
  # machine readable representation
  class Block
    attr_reader :target, :start, :feed
    attr_reader :delta, :spindle_rate
    attr_accessor :type, :profile, :dt, :length
    
    # Creates a new instance with default values
    # @param [String] line A valid G-code line
    def initialize(line = "G00 X0 Y0 Z0 F1000")
      @target = Point[nil, nil, nil]
      @feed   = nil
      @spindle_rate = nil
      @type   = nil
      @profile = nil
      @dt      = 0.0
      @length  = nil
      @delta   = nil
      self.line = line
    end
    
    # Saves a given string representing a G-code line to @line and parses it.
    # @param [String] l A valid G-code line
    def line=(l)
      @line = l.upcase
      self.parse
    end
    
    # Parses the @line string, splitting it in tokens, interpreting each token,
    # and updating the instance variables to relevan values.
    def parse
      tokens = @line.split
      @type = tokens.shift.to_sym
      raise "Unimplemented command #{@type}" unless COMMANDS.include? @type
      
      tokens.each do |t| # T = "X100.3"
        cmd, arg = t[0], t[1..-1].to_f
        case cmd
        when "X", "Y", "Z"
          @target[cmd] = arg
        when "S"
          @spindle_rate = arg
        when "F"
          @feed = arg
        else
          raise "Unknown command #{cmd}"
        end
      end
    end
    
    # Inherits the status of a previous block for unset parameters in the 
    # current block.
    # @param [Block] previous The previous block from which unset params have to
    #        be copied.
    def modal!(previous)
      @start = previous.target
      @target.modal!(@start)
      @feed = (@feed or previous.feed)
      @spindle_rate = (@spindle_rate or previous.spindle_rate)
      @length = @target - @start
      @delta  = @target.delta(@start)
      return self
    end
    
    # Returns a nicely formatted Block description.
    # @return [String] The block description.
    def inspect
      "[#{@type} #{@target.inspect} F:#{@feed}, S:#{@spindle_rate}]"
    end
  end #class Block
  
  
  # Class for reading a whole G-code file and 
  # converting it in an Array of Blocks.
  # @example Parser usage:
  #     parser = RNC::Parser.new(cfg)
  #     parser.parse_file
  #     parser.each_block do |block, interp|
  #       p block
  #       p interp
  #     end
  class Parser
    attr_reader :blocks, :file_name
    def initialize(cfg)
      @cfg       = cfg
      @blocks    = [Block.new]
      @file_name = @cfg[:file_name]
      @profiler  = Profiler.new(@cfg)
    end
    
    # Reads a G-code file, and creates a new block for each non-empty line
    # adding it to the {Parser#blocks} array.
    # @return [Fixnum] The number of parsed blocks.
    def parse_file
      puts ">> Parsing file #{@file_name}"
      File.foreach(@file_name) do |l|
        puts "Parsing block #{l}"
        next if l.size <= 1
        b         = Block.new(l).modal!(@blocks[-1])
        b.profile = @profiler.velocity_profile(b.feed, b.length)
        b.dt      = @profiler.dt
        @blocks << b
      end
      @blocks.shift
      return @blocks.size - 1
    end
    
    # Loops on each element of the {Parser#blocks} array, calling the passed 
    # ruby-block (i.e. a +do...end+ construct) for each element, and passing to it the {Block} instance
    # and its {Interpolator}
    # @yield [block, interp] Gives the current {Block} and its {Interpolator} to
    #        the passed ruby-block.
    # @yieldparam [Block] block A {Block} instance
    # @yieldparam [Interpolator] an {Interpolator} instance built on the 
    #             current block parameters.
    def each_block
      interp = Interpolator.new(@cfg)
      @blocks.each do |block|
        interp.block = block
        yield block, interp
      end
    end
    
  end #class Parser
  
  
  # Class for calculating the velocity profile
  # and the lambda function for a given Block
  class Profiler
    attr_reader :dt, :accel, :feed, :times
    def initialize(cfg)
      @cfg = cfg # It will hold values for max A and max D
      @dt  = 0.0
    end
    
    # Calculates the velocity profile given a nominal feed value f_m and a 
    # length l. The profile is returned in the form of a callable Proc object, 
    # which in turn requires an evaluation time (between 0 and the block 
    # duration) and returns a Hash with :lambda and :type keys. The :type key
    # is :A for acceleration, :M for maintenance, :D for deceleration.
    # @example Basic usage (when +block+ is a {Block} instance):
    #     result = block.profile.call(t)
    #     p result[:type]   # => :A, means that we're accelerating
    #     p result[:lambda] # => 0.33, means we have covered the 33% of the segment length
    # @param [Numeric] f_m The nominal feedrate
    # @param [Numeric] l The length of the positioning segment
    # @return [Proc] A Proc object.
    def velocity_profile(f_m, l)
      f_m = f_m.to_f
      l   = l.to_f
      
      dt_1 = f_m / @cfg[:A]
      dt_2 = f_m / @cfg[:D]
      dt_m = l / f_m - (dt_1 + dt_2) / 2.0
      
      if dt_m > 0 then # TRAPEZOIDAL PROFILE
        q = quantize(dt_1 + dt_m + dt_2, @cfg[:tq])
        dt_m += q[1]
        f_m = (2 * l) / (dt_1 + dt_2 + 2 * dt_m)
      else             # TRIANGULAR PROFILE
        dt_1 = Math::sqrt(2*l / (@cfg[:A] + @cfg[:A]**2 / @cfg[:D]))
        dt_2 = dt_1 * @cfg[:A] / @cfg[:D]
        q = quantize(dt_1 + dt_2, @cfg[:tq])
        dt_m = 0.0
        dt_2 += q[1]
        f_m = 2 * l / (dt_1 + dt_2)
      end
      a = f_m / dt_1
      d = - (f_m / dt_2)
      
      @times = [dt_1, dt_m, dt_2]
      @feed  = f_m
      @accel = [a, d]
      @dt    = q[0]
      
      return proc do |t|
        r = 0.0
        if t < dt_1 then
          type = :A
          r = a * t ** 2 / 2
        elsif t < dt_1 + dt_m
          type = :M
          r = f_m * dt_1 / 2.0 + f_m * (t - dt_1)
        else
          type = :D
          t_2 = dt_1 + dt_m
          r = f_m * dt_1 / 2.0 + f_m * (dt_m + t - t_2) + d / 2.0 * (t ** 2 + t_2 ** 2) - d * t * t_2
        end
        {:lambda => r / l, :type => type}
      end # proc
    end # velocity_profile method
        
    private
    def quantize(t, dt)
      result = []
      if (t % dt) == 0.0 then
        result = [t, 0.0]
      else
        result[0] = ((t / dt).to_i + 1) * dt
        result[1] = result[0] - t
      end
      return result
    end
    
  end #class Profiler
  
  
  # Class for interpolating a lambda function to 
  # the axes involved in a tool motion
  class Interpolator
    attr_accessor :block
    
    # Initializes a new Interpolator.
    # @param [Hash] cfg A configuration hash.
    def initialize(cfg)
      @cfg   = cfg
      @block = nil
    end
    
    # Loops on the current block, with a given timestep tq or with the timestep
    # provided within the configuration hash.
    # @example Basic usage:
    #       parser = RNC::Parser.new(cfg)
    #       parser.parse_file
    #       parser.each_block do |block, interp|
    #         interp.each_timestep do |t, cmd|
    #           p [t, cmd] 
    #         end
    #       end
    # @param [Float] tq The quantization time
    # @yield [t, cmd] Per every tq time step, calls the ruby-block
    # @yieldparam [Float] t The time elapsed since the block start
    # @yieldparam [Hash] cmd A Hash with keys :position, :lambda, :type.
    def each_timestep(tq=@cfg[:tq])
      raise "Need a block" unless block_given?
      t = 0.0
      while (cmd = self.eval(t)) do
        yield t, cmd
        t += tq
      end
    end
    
    # Evaluates the velocity profile of current block and returns a {Point}
    # with the three projections on X, Y, and Z.
    # @param [Float] t The current time.
    # @return [Hash] A Hash with keys :position, :lambda, :type.
    def eval(t)
      if t > @block.dt + 10*Float::EPSILON then
        return nil
      end
      result = {}
      case @block.type
      when :G00
        result[:position] = @block.target
        result[:lambda]   = 0.0
        result[:type]     = :R
      when :G01
        result = @block.profile.call(t)
        result[:position] = Point[]
        [:X, :Y, :Z].each do |axis|
          result[:position][axis] = @block.start[axis] + result[:lambda] * @block.delta[axis]
        end
      else
        raise "Unimplemented command #{@block.type}"
      end
      return result
    end
    
  end #class Interpolator
  

end #module RNC



