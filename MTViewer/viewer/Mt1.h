//
//  Mt1.h
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#ifndef __viewer__Mt1__
#define __viewer__Mt1__

#include "viewer.hh"

class MachineTool1 : public MachineTool{
public:
  void display();
  void init_objects();
  void describe();
  void translate_axes();
  void plot_point(point_t point, GLfloat size);
  
  MachineTool1();

};

#endif /* defined(__viewer__Mt1__) */
