//
//  LinearAxis.h
//  viewer
//
//  Created by Paolo Bosetti on 1/7/13.
//  Copyright (c) 2013 UniTN. All rights reserved.
//

#ifndef __viewer__LinearAxis__
#define __viewer__LinearAxis__

#include <iostream>
#include "Prism.h"

using namespace std;

typedef struct {
  float mass;
} dynamics_t;

typedef struct {
  GLfloat angle, x, y, z;
} rot_t;

class LinearAxis {
  string name;
  Prism way, start_block, end_block, cursor;
  GLfloat length, width, height;
  GLfloat cursor_length, cursor_height;
  GLfloat position, limiter_left, limiter_right;
  point_t in_flange;
  point_t out_flange_position;
  dynamics_t params;
  rot_t rotation;
  bool fixed_cursor;
  
public:
  LinearAxis(string s="");
  ~LinearAxis();
  void setup();
  void draw();

  point_t out_flange();

  GLfloat get_cursor_height();

  void set_color(GLfloat R, GLfloat G, GLfloat B, GLfloat T);
  void set_length(GLfloat val);
  void set_width(GLfloat val);
  void set_height(GLfloat val);
  void set_cursor_length(GLfloat val);
  void set_cursor_height(GLfloat val);
  void set_position(GLfloat val);
  void set_rotation(GLfloat a, GLfloat x, GLfloat y, GLfloat z);
  void set_in_flange(point_t p);
  void set_in_flange(GLfloat x, GLfloat y, GLfloat z);
  void set_out_flange_position(GLfloat x, GLfloat y, GLfloat z);
  void set_fixed_cursor(bool val);
  void set_limiters(GLfloat l, GLfloat r);

private:
  void draw_frame();
  void draw_frame(point_t const *p);
};


#endif /* defined(__viewer__LinearAxis__) */
