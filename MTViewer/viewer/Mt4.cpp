//
//  Mt1.cpp
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#include "Mt4.h"

MachineTool4::MachineTool4() : MachineTool()
{
  name = "Bosch";
  
  range[0] = 800;
  range[1] = 400;
  range[2] = 400;

  x_dim[0] = 800;
  x_dim[1] = 200;
  x_dim[2] = 100;

  y_dim[0] = 500;
  y_dim[1] = 100;
  y_dim[2] = 100;

  z_dim[0] = 400;
  z_dim[1] = 100;
  z_dim[2] = 100;

  x_axis = LinearAxis("X");
  x_axis.set_length(x_dim[0]);
  x_axis.set_width(x_dim[1]);
  x_axis.set_height(x_dim[2]);
  x_axis.set_out_flange_position(0, -y_dim[1]/2.0, 0.0);
  x_axis.setup();

  y_axis = LinearAxis("Y");
  y_axis.set_length(y_dim[0]);
  y_axis.set_width(y_dim[1]);
  y_axis.set_height(y_dim[2]);
  y_axis.set_color(0.0, 1.0, 0, 0.8);
  y_axis.set_fixed_cursor(true);
  y_axis.set_rotation(180, 1, -1, 0);
  y_axis.set_out_flange_position(0, y_dim[1]/2.0, y_dim[2]/2.0);
  y_axis.set_limiters(0, 100);
  y_axis.setup();

  z_axis = LinearAxis("Z");
  z_axis.set_length(z_dim[0]);
  z_axis.set_width(z_dim[1]);
  z_axis.set_height(z_dim[2]);
  z_axis.set_color(0.0, 0.0, 1.0, 0.8);
  z_axis.set_rotation(120, 1, -1, 1);
  z_axis.set_fixed_cursor(true);
  z_axis.set_out_flange_position(z_dim[1]/2.0, 0.0, z_dim[2]/2.0);
  z_axis.setup();
}

void MachineTool4::translate_axes()
{
  
}

void MachineTool4::plot_point(point_t point, GLfloat size)
{
  glPushMatrix();
  glRotatef(180.0, 0, 1, 0);
  glRotatef(-90.0, 1, 0, 0);
  glColor4f(1,1,1,0);
  glTranslatef(point.x, point.y, point.z-tool_length);
  glutSolidSphere(size, 8, 8);
  glPopMatrix();
}

void MachineTool4::display()
{
  glPushMatrix();
  glRotatef(180.0, 0, 1, 0);
  glRotatef(-90.0, 1, 0, 0);

  if (draw_box) {
    volume.set_dimension(range[0], range[1], range[2]-tool_length);
    volume.set_wire_only(true);
    volume.set_color(1, 1, 1, 1);
    volume.set_handle(-range[0]/2.0, -range[1]/2.0, -(range[2]-tool_length)/2.0);
    volume.draw();
  }

  // Machine tool
  glPushMatrix();
  glTranslatef(
               y_dim[1]/2.0 + z_dim[2]/2.0 + z_axis.get_cursor_height(),
               range[1] + x_dim[1]/2.0 + z_dim[1]/2.0,
               range[2] - x_dim[2] - x_axis.get_cursor_height() - y_axis.get_cursor_height()
  );
  x_axis.set_position(coord.x);
  x_axis.draw();

  y_axis.set_in_flange(x_axis.out_flange());
  y_axis.set_position(coord.y);
  y_axis.draw();

  z_axis.set_in_flange(y_axis.out_flange());
  z_axis.set_position(coord.z);
  z_axis.draw();
  /* Table */

  /* Tool */
  point_t p = z_axis.out_flange();
  GLUquadric *quad;
  quad = gluNewQuadric();
  glColor4f(1,0.5,0,1);
  glTranslatef(p.x, p.y, p.z);
  glRotatef(90, 0, 1, 0);
  glPushMatrix();
  gluCylinder(quad, tool_radius, tool_radius, tool_length - (tool_ballpoint ? tool_radius : 0.0), 20, 1);
  glTranslatef(0.0, 0.0, tool_length - (tool_ballpoint ? tool_radius : 0.0));
  glScalef(1.0, 1.0, (tool_ballpoint ? 1.0 : 0.0));
  gluSphere(quad, tool_radius, 20, 20);
  glPopMatrix();

  glPopMatrix(); // end machine tool

  
  if (draw_axes) {
    glPushMatrix();
    glLineWidth(3.0);
    glColor4f(1, 0, 0, 0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(axes_length,0,0);
    glEnd();
    glPushMatrix();
    glTranslatef(axes_length,0,0);
    glRotatef(90,0,1,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glPopMatrix();

    glColor4f(0,1,0,0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(0,axes_length,0);
    glEnd();
    glPushMatrix();
    glTranslatef(0,axes_length,0);
    glRotatef(-90,1,0,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glPopMatrix();

    glColor4f(0,0,1,0.66);
    glBegin(GL_LINES);
    glVertex3f(0,0,0);
    glVertex3f(0,0,axes_length);
    glEnd();
    glPushMatrix();
    glTranslatef(0,0,axes_length);
    glRotatef(0,0,1,0);
    glutSolidCone(axes_length/10.0, axes_length/5.0, 20, 1);
    glLineWidth(1.0);
    glPopMatrix();
    glPopMatrix();
  }

  glPopMatrix();

  glLineWidth(1.0);

}


void MachineTool4::init_objects()
{
}

