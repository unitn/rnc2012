//
//  Mt3.h
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#ifndef __viewer__Mt3__
#define __viewer__Mt3__

#include "viewer.hh"

class MachineTool3 : public MachineTool{
public:
  void display();
  void init_objects();
  void describe();
  void translate_axes();
  void plot_point(point_t point, GLfloat size);
  
  MachineTool3();

};

#endif /* defined(__viewer__Mt3__) */
