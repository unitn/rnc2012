//
//  viewer.hh
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#ifndef viewer_viewer_hh
#define viewer_viewer_hh

#ifdef __APPLE__
#include <OpenGL/gl.h>     // The GL Header File
#include <GLUT/glut.h>   // The GL Utility Toolkit (Glut) Header
#else
#include <gl.h>     // The GL Header File
#include <glut.h>   // The GL Utility Toolkit (Glut) Header
#endif

#include <iostream>
#include <string>
#include "Prism.h"


using namespace std;

typedef struct {
  char flag;
  bool run;
  GLfloat coord[3];
  GLfloat tool_length;
} command_t;

typedef struct
{
  GLfloat center[3];
  GLfloat dims[3];
  GLfloat handle[3];
} object_placing_t;

class MachineTool {
public:
  string name;
  bool draw_box;
  bool draw_axes;
  GLfloat axes_length;
  GLfloat tool_length;
  GLfloat tool_radius;
  bool tool_ballpoint;
  GLfloat range[3];
  point_t coord;

  virtual void display() =0;
  virtual void init_objects() =0;
  virtual void translate_axes() =0;
  virtual void plot_point(point_t point, GLfloat size) =0;
  
  GLfloat envelope()
  {
    GLfloat size = 0.0;
    for (int i = 0; i<3; i++)
      size = x_dim[i] > size ? x_dim[i] : size;
    for (int i = 0; i<3; i++)
      size = y_dim[i] > size ? y_dim[i] : size;
    for (int i = 0; i<3; i++)
      size = z_dim[i] > size ? z_dim[i] : size;

    return size;
  }
  
  void describe()
  {
    std::cout << "This is Machine Tool mod. " << name << ", envelope: " << envelope() << std::endl;
    std::cout << "Draw box: " << (draw_box ? "yes" : "no") << std::endl;
  }

protected:
  GLfloat x_dim[3];
  GLfloat y_dim[3];
  GLfloat z_dim[3];
  GLfloat head_dim[3];
  GLfloat table_dim[3];

  object_placing_t *body_X;
  object_placing_t *body_Y;
  object_placing_t *body_Z;
  object_placing_t *body_head;
  object_placing_t *body_volume;
  object_placing_t *body_table;

  void place(object_placing_t *op)
  {
    GLfloat c[3];
    for (int i = 0; i < 3; i++)
      c[i] = op->center[i] - op->handle[i];
    glTranslatef(c[0], c[1], c[2]);
    glScalef(op->dims[0], op->dims[1], op->dims[2]);
  }

  MachineTool()
  {
    tool_length = 30.0;
    tool_radius = 5.0;
    tool_ballpoint = false;
    draw_box = true;
    draw_axes = true;
    axes_length = 50.0;

    body_X      = new object_placing_t;
    body_Y      = new object_placing_t;
    body_Z      = new object_placing_t;
    body_head   = new object_placing_t;
    body_volume = new object_placing_t;
    body_table  = new object_placing_t;
  }

};

#endif


