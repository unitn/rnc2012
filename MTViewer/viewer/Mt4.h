//
//  Mt1.h
//  viewer
//
//  Created by Paolo Bosetti on 12/17/12.
//  Copyright (c) 2012 UniTN. All rights reserved.
//

#ifndef __viewer__Mt4__
#define __viewer__Mt4__

#include "viewer.hh"
#include "LinearAxis.h"

class MachineTool4 : public MachineTool{
public:
  void display();
  void init_objects();
  void describe();
  void translate_axes();
  void plot_point(point_t point, GLfloat size);
  Prism volume;
  LinearAxis x_axis, y_axis, z_axis;
  
  MachineTool4();

};

#endif /* defined(__viewer__Mt4__) */
