/*
When creating your project, uncheck OWL,
uncheck Class Library, select Static
instead of Dynamic and change the target
model to Console from GUI.
Also link glut.lib to your project once its done.
*/
#include <stdlib.h>
#include <stdio.h>
#include <vector>

#ifdef __APPLE__
#include <OpenGL/gl.h>     // The GL Header File
#include <GLUT/glut.h>   // The GL Utility Toolkit (Glut) Header
#else
#include <gl.h>     // The GL Header File
#include <glut.h>   // The GL Utility Toolkit (Glut) Header
#endif

#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <time.h>

#include "viewer.hh"
#include "Mt1.h"
#include "Mt2.h"
#include "Mt3.h"
#include "Mt4.h"
#include "AntTweakBar.h"

command_t *g_command = new command_t;
uint16_t g_width, g_height;
point_t g_coord;
GLint g_mouse_button;
GLfloat g_fxDiff = -135;
GLfloat g_fYDiff = 30;
GLfloat g_fZDiff = 0;
GLfloat g_xLastIncr = 0;
GLfloat g_yLastIncr = 0;
GLfloat g_fScale = 1.0;
GLfloat g_xLast = -1;
GLfloat g_yLast = -1;
GLint g_bmModifiers = 0;
GLfloat g_incr = 10;
GLfloat g_x_pan = 0.0;
GLfloat g_y_pan = 0.0;
GLfloat g_LightMultiplier = 1.0;
GLfloat g_LightDirection[] = { -0.57735f, -0.57735f, -0.57735f };
GLfloat g_ClearColor[] = {0.0f, 0.1f, 0.3f, 0.5f};
bool g_lighting = true;
static enum {
  HUD_hidden = 0,
  HUD_top,
  HUD_bottom
  } g_head_up_display;
TwBar *g_bar; // Pointer to the tweak bar

vector<point_t> g_points;
GLfloat g_points_size = 2.0;
bool g_show_points = true;
MachineTool * g_mt1 = new MachineTool1();
MachineTool * g_mt2 = new MachineTool2();
MachineTool * g_mt3 = new MachineTool3();
MachineTool * g_mt4 = new MachineTool4();
MachineTool * g_mt = g_mt4;

//void rescale_view();

void
output(int x, int y, string *s)
{
  int len, i;
  len = (int) s->length();
  glRasterPos3i(x, y, 0);
  for (i = 0; i < len; i++) {
    glutBitmapCharacter(GLUT_BITMAP_8_BY_13, s->at(i));
  }
}

void display ( void )   // Create The Display Function
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_ACCUM_BUFFER_BIT);
  glClearColor(g_ClearColor[0], g_ClearColor[1], g_ClearColor[2], g_ClearColor[3]);
  glClearDepth(100.0);
  
  if (g_head_up_display == HUD_bottom) TwDraw();

  // Setup 3D scene:
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_NORMALIZE);

  float v[4]; 
  if (g_lighting) {
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    v[0] = v[1] = v[2] = g_LightMultiplier*0.4f; v[3] = 1.0f;
    glLightfv(GL_LIGHT0, GL_AMBIENT, v);
    v[0] = v[1] = v[2] = g_LightMultiplier*0.8f; v[3] = 1.0f;
    glLightfv(GL_LIGHT0, GL_DIFFUSE, v);
    v[0] = -g_LightDirection[0]; v[1] = -g_LightDirection[1]; v[2] = -g_LightDirection[2]; v[3] = 0.0f;
    glLightfv(GL_LIGHT0, GL_POSITION, v);
  }
  else {
    glDisable(GL_LIGHTING);
  }

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(65.0,(GLfloat)g_width/(GLfloat)g_height,1.0f,20000.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glTranslatef(0.0, -g_mt->envelope()/4.0, -2.0 * g_mt->envelope()/2.0);
  glTranslatef(g_x_pan, g_y_pan, 0.0);

  // trackball rotation and scaling
  glPushMatrix();

  glRotatef(g_fYDiff, 1,0,0);
  glRotatef(g_fxDiff, 0,1,0);
  glRotatef(g_fZDiff, 0,0,1);
  glScalef(g_fScale, g_fScale, g_fScale);

  g_mt->coord = g_coord;
  g_mt->display();

  if (g_show_points) {
    for (int i =0; i < g_points.size(); i++) {
      g_mt->plot_point(g_points[i], g_points_size);
    }
  }
  
  glPopMatrix();
  // end of 3D scene

  
  // Draw 2D scene
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  gluOrtho2D(0, g_width, 0, g_height);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glColor3f(1, 1, 1);

  char buff[100];
  string s;
  sprintf(buff, "X%8.3f", g_coord.x);
  s = buff;
  output(30, 34, &s);

  sprintf(buff, "Y%8.3f", g_coord.y);
  s = buff;
  output(30, 22, &s);

  sprintf(buff, "Z%8.3f L%7.3f R%7.3f", g_coord.z, g_mt->tool_length, g_mt->tool_radius);
  s = buff;
  output(30, 10, &s);

  glPopMatrix();

  if (g_head_up_display == HUD_top) TwDraw();
  
  glutSwapBuffers();
}


void reshape ( int width , int height )
{
  if (height==0)
    height=1;
  
  g_width = width;
  g_height = height;

  glViewport(0, 0, g_width, g_height);

  // Send the new window size to AntTweakBar
  TwWindowSize(g_width, g_height);
}

void arrow_keys ( int a_keys, int x, int y )
{
  if (TwEventSpecialGLUT(a_keys, x, y))
    return;
  
  switch ( a_keys ) {
    case GLUT_KEY_UP:     // When Up Arrow Is Pressed...
      glutFullScreen ( ); // Go Into Full Screen Mode
      break;
    case GLUT_KEY_DOWN:               // When Down Arrow Is Pressed...
      glutReshapeWindow ( 500, 500 ); // Go Into A 500 By 500 Window
      break;
    default:
      break;
  }
}

void motion(int x, int y)
{
  if (TwEventMouseMotionGLUT(x, y)) {
    g_mt->init_objects();
    return;
  }

  if (g_xLast != -1 || g_yLast != -1) {
    g_xLastIncr = x - g_xLast;
    g_yLastIncr = y - g_yLast;
    if ((g_bmModifiers & GLUT_ACTIVE_CTRL) != 0) {
      if (g_xLast != -1) {
        g_fScale += (g_yLastIncr * 0.01);
        if (g_fScale < 0.1)
          g_fScale = 0.1;
        if (g_fScale > 20)
          g_fScale = 20.0;
       }
    }
    else if (g_mouse_button == GLUT_RIGHT_BUTTON) {
      if (g_xLast != -1) {
        g_x_pan += g_xLastIncr * 2.0;
        g_y_pan += -g_yLastIncr * 2.0;
      }
    }
    else {
      if (g_xLast != -1) {
        g_fxDiff += g_xLastIncr;
        g_fYDiff += g_yLastIncr;
      }
    }
  }
  g_xLast = x;
  g_yLast = y;
  glutPostRedisplay();
}

void mouse(int button, int state, int x, int y)
{
  if (TwEventMouseButtonGLUT(button, state, x, y)) {
    g_mt->init_objects();
    return;
  }

  g_mouse_button = (GLint)button;
  g_bmModifiers = glutGetModifiers();

  if (state == GLUT_UP) {
    g_xLast = -1;
    g_yLast = -1;
  }
  g_xLastIncr = 0;
  g_yLastIncr = 0;
  
  glutPostRedisplay();
}

void idle()
{
  if (g_command->flag == '*') {
    g_coord.x = g_command->coord[0];
    g_coord.y = g_command->coord[1];
    g_coord.z = g_command->coord[2];
    g_points.push_back(g_coord);
    g_mt->tool_length = g_command->tool_length;
    g_command->flag = '\0';
    TwRefreshBar(g_bar);
  }
  usleep(10000);
  glutPostRedisplay();
}

void setup_tw_machine_vars()
{
  TwRemoveAllVars(g_bar);
  TwAddVarRO(g_bar, "Machine name", TW_TYPE_STDSTRING, &(g_mt->name), NULL);
  TwAddVarRW(g_bar, "Tool length", TW_TYPE_FLOAT, &(g_mt->tool_length), " min=0.0 max=200.0 step=1 keyIncr=l keyDecr=L group='Machine' ");
  TwAddVarRW(g_bar, "Tool radius", TW_TYPE_FLOAT, &(g_mt->tool_radius), " min=0.0 max=200.0 step=1 keyIncr=r keyDecr=R group='Machine' ");
//  TwAddVarRW(g_bar, "Range X", TW_TYPE_FLOAT, &(g_mt->range[0]), "step=1 group='Machine' ");
//  TwAddVarRW(g_bar, "Range Y", TW_TYPE_FLOAT, &(g_mt->range[1]), "step=1 group='Machine' ");
//  TwAddVarRW(g_bar, "Range Z", TW_TYPE_FLOAT, &(g_mt->range[2]), "step=1 group='Machine' ");
  TwAddVarRW(g_bar, "Ballpoint", TW_TYPE_BOOLCPP, &(g_mt->tool_ballpoint), "group='Machine'");
  
  TwAddVarRW(g_bar, "X", TW_TYPE_FLOAT, &(g_coord.x), " group='Coordinates'");
  TwAddVarRW(g_bar, "Y", TW_TYPE_FLOAT, &(g_coord.y), " group='Coordinates'");
  TwAddVarRW(g_bar, "Z", TW_TYPE_FLOAT, &(g_coord.z), " group='Coordinates'");

  TwAddVarRW(g_bar, "Draw axes", TW_TYPE_BOOLCPP, &(g_mt->draw_axes), " group='Options'");
  TwAddVarRW(g_bar, "Draw box", TW_TYPE_BOOLCPP, &(g_mt->draw_box), " group='Options'");
  TwAddVarRW(g_bar, "Zoom", TW_TYPE_FLOAT, &g_fScale, " min=0.2 max=15.0 step=0.01 group='Options'");
  TwAddVarRW(g_bar, "Points size", TW_TYPE_FLOAT, &(g_points_size), "min=0 max=10 step=0.1 group='Options'");
  TwAddVarRW(g_bar, "Show points", TW_TYPE_BOOLCPP, &(g_show_points), "group='Options'");
  TwAddVarRW(g_bar, "Clear color", TW_TYPE_COLOR4F, &(g_ClearColor), " group='Options'");

  TwAddVarRW(g_bar, "Lighting", TW_TYPE_BOOLCPP, &(g_lighting), " group='Light'");
  TwAddVarRW(g_bar, "Light direction", TW_TYPE_DIR3F, &g_LightDirection, " group='Light'");
  TwAddVarRW(g_bar, "Light multiplier", TW_TYPE_FLOAT, &(g_LightMultiplier), " min=0 max=2 step=0.1 group='Light'");

}

void keyboard ( unsigned char key, int x, int y )
{
  bool move = false;
  if (TwEventKeyboardGLUT(key, x, y) != 0)
    return;
  else
    switch ( key ) {
      case '1':
        g_mt = g_mt1;
        g_mt->init_objects();
        g_mt->describe();
        setup_tw_machine_vars();
        break;
      case '2':
        g_mt = g_mt2;
        g_mt->init_objects();
        g_mt->describe();
        setup_tw_machine_vars();
        break;
      case '3':
        g_mt = g_mt3;
        g_mt->init_objects();
        g_mt->describe();
        setup_tw_machine_vars();
        break;
      case '4':
        g_mt = g_mt4;
        g_mt->init_objects();
        g_mt->describe();
        setup_tw_machine_vars();
        break;
      case 'q':
        g_command->run = false;
        exit(0);
        break;
      case 'a':
        g_mt->draw_axes = not g_mt->draw_axes;
        break;
      case 'b':
        g_mt->draw_box = not g_mt->draw_box;
        break;
      case 'x':
        g_coord.x += g_incr;
        move = true;
        break;
      case 'X':
        g_coord.x -= g_incr;
        move = true;
        break;
      case 'y':
        g_coord.y += g_incr;
        move = true;
        break;
      case 'Y':
        g_coord.y -= g_incr;
        move = true;
        break;
      case 'z':
        g_coord.z += g_incr;
        move = true;
        break;
      case 'Z':
        g_coord.z -= g_incr;
        move = true;
        break;
      case 'p':
        printf("X %.3f Y%.3f Z%.3f\n", g_coord.x, g_coord.y, g_coord.z);
        printf("Draw axes: %s, draw box: %s\n", g_mt->draw_axes ? "yes" : "no", g_mt->draw_box ? "yes" : "no");
        g_mt->describe();
        break;
      case 'h':
        switch (g_head_up_display) {
          case HUD_bottom:
            g_head_up_display = HUD_hidden;
            break;
          case HUD_hidden:
            g_head_up_display = HUD_top;
            break;
          case HUD_top:
            g_head_up_display = HUD_bottom;
            break;
        }
        break;
      case ' ':
        g_command->run = !g_command->run;
        break;
      case 27:        // When Escape Is Pressed...
        g_command->run = false;
        exit ( 0 );   // Exit The Program
        break;        // Ready For Next Case
      default:        // Now Wrap It Up
        break;
    }
  TwRefreshBar(g_bar);
  if (move) {
    g_points.push_back(g_coord);
  }
  glutPostRedisplay();
}

int main ( int argc, char** argv )   
{
  key_t key;
  int shmid = 0;
  shmid_ds *ds = new shmid_ds;
  if (argc == 2) {
    key = atoi(argv[1]);
    printf("Shared memory key: %d (%s)\n", key, argv[1]);
    if ((shmid = shmget(key, sizeof(command_t), IPC_CREAT | 0666)) < 0) {
      perror("shmget");
      exit(1);
    }
    g_command = (command_t*)shmat(shmid, 0, SHM_RND);
  }
  g_command->tool_length = 30.0;
  g_command->run = false;
  g_command->flag = '\0';

  g_mt->init_objects();
  glutInit            ( &argc, argv );
  glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA );
  glutInitWindowSize  ( 500, 500 );
  glutInitWindowPosition(100, 100);
  glutCreateWindow    ( "Machine Tool viewer" );
  glShadeModel(GL_SMOOTH);
  glClearColor(0.0f, 0.1f, 0.3f, 0.5f);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glEnable(GL_LINE_SMOOTH);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDepthFunc(GL_LEQUAL);
  glEnable ( GL_COLOR_MATERIAL );
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  glutDisplayFunc     ( display );
  glutReshapeFunc     ( reshape );

  TwInit(TW_OPENGL, NULL);

  glutKeyboardFunc    ( keyboard );
  glutSpecialFunc     ( arrow_keys );
  glutMotionFunc      ( motion );
  glutMouseFunc       ( mouse );
  glutIdleFunc        ( idle );
  glutPassiveMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);

  TwGLUTModifiersFunc(glutGetModifiers);

  g_bar = TwNewBar("Controls");
  g_head_up_display = HUD_top;
  TwDefine("Controls iconified=true");
  TwDefine("Controls size='200 400' color='96 216 224' "); 

  setup_tw_machine_vars();

  glutMainLoop();
  TwTerminate();
  shmdt(g_command);
  if (shmctl(shmid, IPC_RMID, ds) < 0) {
    perror("shmctl");
    exit(1);
  }
  return 0;
}

