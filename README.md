RNC2012 - Ruby Numerical Control 2012
=====================================

Introduction
------------
This is the repository where the code developed for a simplified computer numerical control written in [Ruby](http://ruby-lang.org) language will be maintained during the course of Manufacturing Automation at UniTN.

**NOTE**: to download the code from BitBucket, go on the [downloads page](https://bitbucket.org/unitn/rnc2012/downloads) and click on the compressed archive you prefer (zip, gz, or bz2) on the master branch (which contains the latest modification). The "tags" section contains the code corresponding to the single lessons.

Pre-requisites
--------------
The development platform is Debian 6.0.x and Ruby 1.9.3-p194 (installed via [RVM](http://rvm.io)).

* Operating system - 
  It is suggested to install Debian either in a dual-boot or under a virtual machine using [VirtualBox](http://www.virtualbox.org). Debian install ISO image can be downloaded from [here](http://ftp.acc.umu.se/debian-cd/current-live/i386/iso-hybrid/debian-live-6.0.5-i386-gnome-desktop.iso)
  **It is advised to install Debian in English language**.
  
* Network connection -
  The install procedure requires a network connection, so be sure to have a working connection under linux. If you are using VirtualBox, the virtual machine will see the network as an ethernet connection (which will be tunneled through whichever connection your host operating system has). **Note**: the real internet connection must not require a proxy (your home connection or the `unitn` WiFi network are fine).
  
* RVM and Ruby - 
  RVM is a manager for Ruby systems. Install it as for the following section.
  

Installing the Ruby environment
-------------------------------
On your Debian system, go on the menu `Applications>Accessories` and open `Root Terminal` and `Terminal`. Opening the `Root Terminal` will ask you for the password. 

You should have two terminal windows, now. One of them has the text `(as superuser)` in the title. We will refer to it as *the root shell* or *the root terminal*.

In the root shell, type the following commands (the `$` indicates the command prompt, you don't have to type it!):
    
    :::bash
    $ sudo apt-get update
    ... wait for the process to conclude, read the messages and if it goes OK type:
    $ apt-get install curl
    
Switch to the user terminal and type the following commands:

    :::bash
    $ echo insecure >> .curlrc
    $ curl -L https://get.rvm.io | bash -s stable --ruby
    
At this point the output stops giving you some instruction on a screen ending with a `:` on the last line. At the bottom of the screen, it tells you that you need to install some debian packages in order to complete the RVM installation. Hit the down arrow a few times until you can read the complete list of packages. Then select the command (from `apt-get install` to the last package name) and paste it into the root terminal, and type return. This will ask you for confirmation (type return) and install a bunch of packages (libraries and compilers).

Once the `apt-get install` process in the root terminal is completed, switch again on the user terminal and type:

    :::bash
    $ source .rvm/scripts/rvm
    $ rvm install 1.9.3
    
This will download the ruby sources and compile them. It will take 5-10 mins.

At the end of the process, you can close the root terminal and type:

    :::bash
    $ ruby --version
    ruby 1.9.3p194 (2012-4-20 revision 35410) [i686-linux]
    
If you got to this point, you are ready to go.


First example
-------------
To create your first example program, create a new folder, move into it, and edit a new file named `example_1.rb`:

    :::bash
    $ mkdir examples
    $ cd examples
    $ gedit example_1.rb &

The last command will open a text editor with a new and empty ruby file. Type the following line into it:

    :::ruby
    puts "Hello, world!"

Then save it, switch to the terminal window and type:

    :::bash
    $ ruby example_1.rb
    Hello, world!
    

Charting and Visualizing
------------------------
For charting the output of algorithms, we will use the GNUPlot software. Install it by typing the following command **in a root shell**:

    :::bash
    $ sudo apt-get install gnuplot
    
GNUPlot can plot both functions and scatter charts of data sets. Data sets are read from text files. The project includes an example data file, data.txt, which can be used for learning how to plot:

    :::bash
    $ gnuplot
    :::gnuplot
    > plot "data.txt" using 1:2 with lines

For visuaizing a 3D schematics of the machine tool, the OpenGL libraries are needed. Type the following **in a root shell**:

    :::bash
    $ sudo apt-get install libglut3-dev
if this does not work, try with
    :::bash
    $ sudo apt-get install freeglut3-dev

then install:
    :::bash
    $ sudo apt-get install libglew1.5-dev
    
    
Then close the root shell and install the required ruby gem in the user shell:

    :::bash
    $ gem install ffi
    $ rake build
    $ rake clean
    
and finally test the viewer:

    :::bash
    $ ./rnc.rb loop.g
