#|/usr/bin/env ruby

require "./lib/parser"
require "./lib/machine"

CFG = {
  :tq        => 0.01
}

CFG_M = {
  :x => {
    :mass => 500, :max_f => 5_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 49_000
  },
  :y => {
    :mass => 350, :max_f => 5_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 34_000
  },
  :z => {
    :mass => 150, :max_f => 2_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 14_000
  }
}

m = RNC::Machine.new(CFG_M)
m.go_to [0,0,0]
m.states = [
  {:x => 0, :v => 0},
  {:x => 0, :v => 0},
  {:x => 0, :v => 0}
]

tq = 0.01
j = 100
t = 0.0
File.open("accel.txt", "w") do |file|
  loop do
    # calculate current time
    t += tq
  
    # Linearly increase the acceleration
    a_t = j * t
    # calculate target position
    s = j * t ** 3 / 6.0
    # set the machine set-point
    m.go_to [s,s,s]
    # update the machine positions (perform forward integration)
    status = m.step(tq)
    # Get the position error
    error = m.error
    # save current data in the file
    file.puts [t, s, a_t, error] * " "
    #exit after 10 s
    break if t > 10
  end
end


