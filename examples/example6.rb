#!/bin/env ruby

# Inheritance


class Polygon
  attr_reader :sides

  def initialize(n)
    @sides = Array.new(n)
  end
  
  def num_sides
    @sides.count
  end
  
  def sides=(ary)
    raise "ary must be an Array!" unless ary.kind_of? Array
    raise "Wrong number of sides!" if ary.size != self.num_sides
    
    @sides = ary
  end
  
  def perimeter
    @sides.inject(0.0) do |sum, i|
      sum + i
    end
  end
end

class Triangle < Polygon
  def initialize
    super(3)
  end
  
  # Methid override
  def inspect
    "Triangle with sides #{@sides}"
  end
end

tri = Triangle.new
tri.sides = [1,2,3]
p tri



