#!/bin/env ruby

# Proc Objects


def test(n)
  return proc do |t|
    t * n
  end
end

f = test(5)
puts f.call(10)
