#!/usr/bin/env ruby


class Chalk
  def initialize(length=10, color="white")
    @length = length
    @color = color
  end
  
  def write(something)
    if @length <= 2 then
      puts "This chalk is done!"
    else
      puts "In #{@color}: #{something}"
      @length = @length - 1
    end
  end
end


red_chalk = Chalk.new(20, "red")
blue_chalk = Chalk.new(5, "blue")

blue_chalk.write("one")
blue_chalk.write("two")
blue_chalk.write("three")
blue_chalk.write("four")
blue_chalk.write("five")
blue_chalk.write("six")
