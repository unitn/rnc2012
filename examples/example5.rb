#!/bin/env ruby

# Inheritance


class Polygon
  attr_reader :num_sides, :sides

  def initialize(n)
    @num_sides = n
    @sides = []
  end
  
  def sides=(ary)
    raise "ary must be an Array!" unless ary.kind_of? Array
    raise "Wrong number of sides!" if ary.size != @num_sides
    
    @sides = ary
  end
  
  def perimeter
    p = 0.0
    @sides.each do |l|
     p = p + l
    end
    return p
  end
end

class Triangle < Polygon
  def initialize
    super(3)
  end
  
end

tri = Triangle.new
tri.sides = [1,2,3]
puts tri.perimeter



