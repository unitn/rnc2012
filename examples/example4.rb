#!/bin/env ruby

# Example on Hash class

#Array class:
ary = ["value",10.0,12.3]
ary[0]

#Hash class
hsh = {
  :name => "value", 
  :radius => 10.0, 
  :length => 12.3
 }
 
puts hsh[:radius]
 
ary.each_with_index do |e,i|
  puts "ary[#{i}] = #{e}"
end
 
hsh.each do |k,v|
  puts "hsh[#{k}] => #{v}"
end

3.upto(7) do |i|
  puts i
end

(1...15).each do |i|
    
end

puts (1...15).include? 15
puts (1..15).include? 15
puts (1..15).to_a


