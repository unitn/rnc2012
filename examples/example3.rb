#!/usr/bin/env ruby

class Person
  attr_accessor :name, :surname, :age, :address
  
  def initialize(name)
    @name = name
    greet = "ciao"
    self.greet
  end
    
  def greet
    puts "Hello, my name is #{self.name}!"
  end
  
end


paolo = Person.new("Paolo")
paolo.greet

luca = Person.new("Luca")
luca.greet
