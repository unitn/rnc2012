#!/usr/bin/env ruby

require "./lib/parser"
require "./lib/machine"
require "./MTViewer/viewer.rb"

# Constants for configuration:
COLORS = {A:1, M:2, D:3, R:0}
CFG = {
  :file_name => (ARGV[0] or "test.g"),
  :A         => 400,
  :D         => 400,
  :tq        => 0.01
}

CFG_M = {
  :x => {
    :mass => 500, :max_f => 5_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 49_000
  },
  :y => {
    :mass => 350, :max_f => 5_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 34_000
  },
  :z => {
    :mass => 150, :max_f => 2_000_000, :res => 2_000,
    :p => 200_000, :i => 10, :d => 14_000
  }
}
MAX_ERROR = 0.005

# Machine tool simulator
m = RNC::Machine.new(CFG_M)
m.go_to [0,0,0]
m.states = [
  {:x => 0, :v => 0},
  {:x => 0, :v => 0},
  {:x => 0, :v => 0}
]

# Machine tool viewer
viewer = Viewer::Link.new("./MTViewer/MTviewer")

# Parsing and excution
puts "contents of #{CFG[:file_name]}:"
parser = RNC::Parser.new(CFG)
parser.parse_file

# Helper function for printing formatted data
def log(t, cmd, status)
  # Print information:
  info = [
    t,
    COLORS[cmd[:type]], 
    cmd[:lambda],
    cmd[:position][:X],
    cmd[:position][:Y],
    cmd[:position][:Z],
    status[0][:x],
    status[1][:x],
    status[2][:x]
  ]
  "%.4f %d %.5f %.3f %.3f %.3f %.3f %.3f %.3f\n" % info 
end

puts "=" * 79
puts "Press SPACE in viewer window. Type 'Q' on the viewer window to close it."
puts "=" * 79
loop until (viewer.run)

File.open("profile.txt", "w") do |file|
  file.puts "# Time Type Lambda Xn Yn Zn X Y Z"
  cum_t = 0.0
  parser.each_block do |block, interp|
    case block.type
    when :G00
      m.go_to block.target
      cmd = {
        :type     => :R,
        :lambda   => 0.0,
        :position => block.target
      }
      begin
        sleep_thread = Thread.new { sleep CFG[:tq]}
        
        status = m.step(CFG[:tq])
        error  = m.error
        cum_t += CFG[:tq]
        file.print log(cum_t, cmd, status)
        print log(cum_t, cmd, status)
        viewer.go_to [status[0][:x], status[1][:x], status[2][:x]]
        
        sleep_thread.join
      end while (error >= MAX_ERROR)
    when :G01
      interp.each_timestep do |t, cmd|
        sleep_thread = Thread.new { sleep CFG[:tq]}
        
        m.go_to cmd[:position]
        status = m.step(CFG[:tq])
        file.print log(t + cum_t, cmd, status)
        print log(t + cum_t, cmd, status)
        viewer.go_to [status[0][:x], status[1][:x], status[2][:x]]
                
        sleep_thread.join
      end
      cum_t += block.dt
    else
      warn "Skipping block #{block.line}"
    end
  end
end

puts "=" * 79
puts "Press SPACE in viewer to quit."
puts "=" * 79
loop while viewer.run
viewer.close

